#!/bin/sh

# Get TF data query argument
eval "$(jq -r '@sh "app_url=\(.app_url) init_keys_file_destination=\(.init_keys_file_destination)"')"

# Validate
if test -z "$app_url" || test "$app_url" == "null"; then
  echo "ERROR: 'app_url' query argument is not set"
  exit 1
fi
if test -z "$init_keys_file_destination" || test "$init_keys_file_destination" == "null"; then
  echo "ERROR: 'init_keys_file_destination' query argument is not set"
  exit 1
fi

# Init vault
output="$(VAULT_ADDR=${app_url} vault operator init)"
if test $? -ne 0; then
  # output status to STDOUT
  jq -n --arg status "most likely already initialized" \
      '{"status": $status}'
  exit 0
fi

# debug
# output="$(cat /tmp/dump)"

# Extract values
recovery_key1=$(echo "$output" | sed -n 's/Recovery Key 1: \(.*\)/\1/p')
recovery_key2=$(echo "$output" | sed -n 's/Recovery Key 2: \(.*\)/\1/p')
recovery_key3=$(echo "$output" | sed -n 's/Recovery Key 3: \(.*\)/\1/p')
recovery_key4=$(echo "$output" | sed -n 's/Recovery Key 4: \(.*\)/\1/p')
recovery_key5=$(echo "$output" | sed -n 's/Recovery Key 5: \(.*\)/\1/p')
root_token=$(echo "$output" | sed -n 's/Initial Root Token: \(.*\)/\1/p')

# output keys to $init_keys_destination
jq -n \
    --arg recovery_key1 "$recovery_key1" \
    --arg recovery_key2 "$recovery_key2" \
    --arg recovery_key3 "$recovery_key3" \
    --arg recovery_key4 "$recovery_key4" \
    --arg recovery_key5 "$recovery_key5" \
    --arg root_token "$root_token" \
    --arg status "initialized" \
    '{"root_token": $root_token, "recovery_key1": $recovery_key1, "recovery_key2": $recovery_key2, "recovery_key3": $recovery_key3, "recovery_key4": $recovery_key4, "recovery_key5": $recovery_key5, "status": $status}' \
    > "${init_keys_file_destination}"

# output status to STDOUT
jq -n --arg status "initialized" \
    '{"status": $status}'
