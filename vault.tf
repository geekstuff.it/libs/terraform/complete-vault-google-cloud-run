locals {
  vault_docker_source              = "vault:1.9.4"
  vault_docker_gcr                 = "gcr.io/${google_project.project.project_id}/${local.vault_docker_source}"
  vault_hostname                   = var.vault_hostname
  vault_url                        = "https://${local.vault_hostname}"
  vault_init_keys_file_destination = "${var.vault_init_keys_destination}/vault-init-keys.${var.vault_project_name}.json"

}

data "google_dns_managed_zone" "dns_zone" {
  name    = var.gcloud_dns_zone
  project = var.dns_project_id
}

resource "null_resource" "docker_vault_pull" {
  provisioner "local-exec" {
    command = "docker pull ${local.vault_docker_source}"
  }
}

resource "null_resource" "docker_vault_tag" {
  provisioner "local-exec" {
    command = "docker tag ${local.vault_docker_source} ${local.vault_docker_gcr}"
  }
  depends_on = [null_resource.docker_vault_pull]
}

resource "null_resource" "docker_vault_push" {
  provisioner "local-exec" {
    command = "docker push ${local.vault_docker_gcr}"
  }
  depends_on = [google_project_service.service, null_resource.docker_vault_tag]
}

module "vault" {
  source = "git::https://gitlab.com/geekstuff.it/libs/terraform/vault-google-cloud-run?ref=v0.2.0"

  name           = "vault-${var.vault_project_name}"
  project        = google_project.project.project_id
  location       = var.region
  vault_image    = local.vault_docker_gcr
  vault_ui       = true
  vault_api_addr = local.vault_url

  # for now..
  bucket_force_destroy = true

  depends_on = [google_project.project, google_project_service.service, null_resource.docker_vault_push]
}

resource "time_sleep" "wait_vault" {
  create_duration = "10s"
  depends_on      = [module.vault]
}

data "external" "vault_operator_init" {
  program = ["bash", abspath("${path.module}/vault-operator-init.sh")]
  query = {
    app_url                    = module.vault.app_url
    init_keys_file_destination = local.vault_init_keys_file_destination
  }
  depends_on = [time_sleep.wait_vault]
}

output "init_status" {
  sensitive = false
  value     = data.external.vault_operator_init.result.status
}

output "init_keys_destination" {
  sensitive = false
  value     = local.vault_init_keys_file_destination
}

resource "google_cloud_run_domain_mapping" "domain_mapping" {
  location   = var.region
  name       = local.vault_hostname
  project    = google_project.project.project_id
  depends_on = [module.vault]

  metadata {
    namespace = google_project.project.project_id
  }

  spec {
    force_override = true
    route_name     = "vault-${var.vault_project_name}"
  }
}

resource "google_dns_record_set" "dns" {
  name         = "${local.vault_hostname}."
  type         = "CNAME"
  ttl          = 1800
  managed_zone = data.google_dns_managed_zone.dns_zone.name
  depends_on   = [google_cloud_run_domain_mapping.domain_mapping]
  project      = var.dns_project_id
  rrdatas      = [google_cloud_run_domain_mapping.domain_mapping.status[0].resource_records[0].rrdata]
}
