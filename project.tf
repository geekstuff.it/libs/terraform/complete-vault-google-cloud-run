provider "google" {
  region                = var.region
  user_project_override = true
}

resource "random_id" "id" {
  byte_length = 4
  prefix      = "vault-${var.vault_project_name}"
}

resource "google_project" "project" {
  name            = "vault-${var.vault_project_name}"
  project_id      = random_id.id.hex
  billing_account = var.billing_account
  folder_id       = var.folder_id
}

resource "null_resource" "cli_enable_service_usage_api" {
  provisioner "local-exec" {
    command = "gcloud services enable serviceusage.googleapis.com --project ${google_project.project.project_id}"
  }

  depends_on = [google_project.project]
}

resource "time_sleep" "wait_service_usage" {
  create_duration = "10s"
  depends_on      = [null_resource.cli_enable_service_usage_api]
}

resource "null_resource" "cli_enable_cloudresourcemanager_api" {
  provisioner "local-exec" {
    command = "gcloud services enable cloudresourcemanager.googleapis.com --project ${google_project.project.project_id}"
  }

  depends_on = [google_project.project, time_sleep.wait_service_usage]
}

resource "time_sleep" "wait_cloudresourcemanager" {
  create_duration = "10s"
  depends_on      = [null_resource.cli_enable_cloudresourcemanager_api]
}

resource "google_project_service" "service" {
  for_each = toset([
    "cloudkms.googleapis.com",
    "iam.googleapis.com",
    "run.googleapis.com",
  ])

  service = each.key

  project            = google_project.project.project_id
  disable_on_destroy = true
  depends_on         = [google_project.project, time_sleep.wait_cloudresourcemanager]
}
