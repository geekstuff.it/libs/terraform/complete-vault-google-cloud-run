# Project
output "gcp_project_id" {
  value = google_project.project.project_id
}

# Vault
output "cloud_run_url" {
  value = module.vault.app_url
}

output "custom_url" {
  value = local.vault_url
}

output "domain_mapping_content" {
  value = google_cloud_run_domain_mapping.domain_mapping.status[0].resource_records[0].rrdata
}

output "custom_dns_info" {
  value = {
    name    = google_dns_record_set.dns.name,
    type    = google_dns_record_set.dns.type,
    rrdatas = google_dns_record_set.dns.rrdatas,
  }
}

output "cloud_run_domain_mapping_content" {
  value = google_cloud_run_domain_mapping.domain_mapping.status[0].resource_records[0].rrdata
}

# output "domain_mapping_status" {
#   value = google_cloud_run_domain_mapping.domain_mapping.status[0].conditions
# }
